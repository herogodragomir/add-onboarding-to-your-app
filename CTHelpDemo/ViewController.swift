//
//  ViewController.swift
//  CTHelpDemo
//
//  Created by Edy Cu Tjong on 6/16/19.
//  Copyright © 2019 Edy Cu Tjong. All rights reserved.
//

import UIKit
import CTHelp

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func helpTapped(_ sender: Any) {
        showCTHelp()
    }
    
    func showCTHelp() {
        let ctHelp = CTHelp()
        
        // Optional values to set colors
        // ctHelp.ctBgViewColor = .white
        // ctHelp.ctTitleColor = .darkText
        // ctHelp.ctHelpTextColor = .darkGray
        // ctHelp.ctActionButtonBGColor = UIColor(red: 28/255, green: 136/255, blue: 197.255, alpha: 1)
        // ctHelp.ctActionButtonTextColor = .white
        
        ctHelp.new(CTHelpItem(title:"No Text-Image Only",
                              helpText: "",
                              imageName:"icons8-phone"))
        ctHelp.new(CTHelpItem(title:"Text and Image",
                              helpText: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                              imageName:"icons8-search"))
        ctHelp.new(CTHelpItem(title:"No Image-Text Only",
                              helpText: "Eu tempor suscipit dis sed. Tortor velit orci bibendum mattis non metus ornare consequat. Condimentum habitasse dictumst eros nibh rhoncus non pulvinar fermentum. Maecenas convallis gravida facilisis. Interdum, conubia lacinia magnis duis nec quisque.Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                              imageName:""))
        
        // Optional addition of two default cards
        // Use only if you wish to change the strings presented in the two default cards
        // ctHelp.ctWebButtonTitle = ""
        // ctHelp.ctWebHelpText = ""
        // ctHelp.ctContactTitle = ""
        // ctHelp.ctContactHelpText = ""
        // ctHelp.ctIncludeDataText = ""
        // ctHelp.ctContactButtonTitle = ""
        
        // Email data
        // ctHelp.ctEmailSubject = ""
        // ctHelp.ctEmailAttachNote = ""
        // ctHelp.ctEmailBody = ""
        
        // Email alert
        // ctHelp.ctDataAlertTitle = ""
        // ctHelp.ctDataAlertMessage = ""
        // ctHelp.ctDataAlertActionNo = ""
        // ctHelp.ctDataAlertActionYes = ""
        
        ctHelp.appendDefaults(companyName: "Your Company Name", emailAddress: "yourContactEmail@somewhere.com", data: nil, webSite: "https://www.yourWebsite.com", companyImageName: "CompanyLogo")
        
        ctHelp.presentHelp(from: self)
    }

}

